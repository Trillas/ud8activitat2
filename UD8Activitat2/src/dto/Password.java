package dto;

public class Password {
	//Doy los atributos de persona
	private int longitud;
	private String contraseņa;
	
	//Hago todos los constructores
	public Password() {
		this.longitud = 0;
		this.contraseņa = " ";
	}
	
	public Password(int longitd) {
		this.longitud = longitud;
		this.contraseņa = generarContraseņa(longitd);
	}
	//Aqui para generar la contraseņa lo hago dando un numero aleatorio y pasandolo a su correspondiente en ascii
	public static String generarContraseņa(int longitud) {
		int num;
		char[] array = new char[longitud];
		for (int i = 0; i < longitud; i++) {
			num = (int) (Math.random() * (122 - 48 + 1) + 48);
			array[i] = (char) num;
		}
		return String.copyValueOf(array);
	}

	public String toString() {
		return "Password [longitud=" + longitud + ", contraseņa=" + contraseņa + "]";
	}
}
